<?php
include_once("db_pdo.php");


function my_show_db(PDO $bdd, $table) {
	if(!is_string($table))
		yield NULL;

	$table_name = substr($bdd->quote($table), 1, -1);
	$sql_query = "SELECT * FROM ".$table_name.";";
	$results_obj = $bdd->query($sql_query);
	if($results_obj === false)
		yield NULL;

	$results_arr = $results_obj->fetchAll(PDO::FETCH_ASSOC);
	if(count($results_arr) == 0)
		yield NULL;

	foreach ($results_arr as $one_row) {
		$strRow = "";
		foreach ($one_row as $key => $value) {
			$strRow .= "[".$key."]=>[".$value."], ";
		}
		$strRow = substr($strRow, 0, -2);
		$strRow .= "\n";  // ?
		yield $strRow;
	}
}


$dbh = connect_db("localhost", "root", "root", "3306", "gecko");
if($dbh !== false)
{
	$generator = my_show_db($dbh, "users");
	foreach ($generator as $line) {
		echo $line;
	}
}
?>
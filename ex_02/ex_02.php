<?php
function my_password_hash($password)
{
	$salt_int    = rand(1, 99999);
	$salt_str	 = sprintf("%05d", $salt_int);

	$pass_hashed = md5($password.$salt_str);

	return array("hash"=>$pass_hashed, "salt"=>$salt_str);
}

function my_password_verify($password, $salt, $hash)
{
	$pass_hashed = md5($password.$salt); 

	if($hash == $pass_hashed)
		return true;

	return false;
}

?>
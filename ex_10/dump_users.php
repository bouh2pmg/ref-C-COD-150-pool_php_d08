<?php
include_once("db_pdo.php");

const ERROR_LOG_FILE = "errors.log";

const DB_HOST     = "localhost";
const DB_USERNAME = "root";
const DB_PASSWORD = "root";
const DB_PORT = 3306;
const DB_NAME = "gecko";


$dbh = connect_db(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_PORT, DB_NAME);
if($dbh === false)
{
	$err_msg = "Error connection to DB\n";
	echo $err_msg;
	file_put_contents(ERROR_LOG_FILE, $err_msg, FILE_APPEND);
	die();	
}
else
	echo "Connection to DB successful\n";

$nb_args = count($argv);


if($nb_args == 1) // the whole table
{
	$sql_query = "SELECT id,name,email,created_at,role FROM users;";

	try 
	{
		$results = $dbh->query($sql_query);
		if($results === false)
		{
			echo "No results matching your search\n";
			die();
		}

		$mask = "|%-5.5s |%-20.20s |%-30.30s |%-10.10s |%-5.5s | \n";
		printf($mask, "Id", "Name", "Email", "Created_at", "Role");
		foreach($results as $row)
		{
			printf($mask, $row["id"], $row["name"], $row["email"], $row["created_at"], $row["role"]);
		}
	}
	catch (PDOException $e)
	{
		echo "Error MySQL, more infos in ".ERROR_LOG_FILE."\n";
		$err_msg = "PDO ERROR: ".$e->getMessage()." storage in ".ERROR_LOG_FILE."\n";
	    file_put_contents(ERROR_LOG_FILE, $err_msg, FILE_APPEND);
	}

}
else if($nb_args == 4)
{
	$filter = $argv[1];
	$value  = $argv[2];
	$exact  = strtolower($argv[3]) == "true" ? true : false;

	if($filter == "password")
	{
		$err_msg = "Don't try to filter the password, it's not possible\n";
		echo $err_msg;
		file_put_contents(ERROR_LOG_FILE, $err_msg, FILE_APPEND);
		die();	
	}

	$sql_query = "SELECT id,name,email,created_at,role FROM users WHERE ".$filter." LIKE ";
	if($exact)
		$sql_query .= "'".$value."';";
	else
		$sql_query .= "'%".$value."%';";

	try
	{
		$results = $dbh->query($sql_query);
		if($results === false)
		{
			echo "No results matching your search\n";
			die();
		}

		if($results->rowCount()>=1)
		{
			$mask = "|%-5.5s |%-20.20s |%-30.30s |%-10.10s |%-5.5s | \n";
			printf($mask, "Id", "Name", "Email", "Created_at", "Role");
			foreach($results as $row)
			{
				printf($mask, $row["id"], $row["name"], $row["email"], $row["created_at"], $row["role"]);
			}			
		}
		else
		{
			echo "No results matching your search\n";
		}
	}
	catch (PDOException $e)
	{
		echo "Error MySQL, more infos in ".ERROR_LOG_FILE."\n";
		$err_msg = "PDO ERROR: ".$e->getMessage()." storage in ".ERROR_LOG_FILE."\n";
	    file_put_contents(ERROR_LOG_FILE, $err_msg, FILE_APPEND);
	}	
}
else
{
	$err_msg = "Bad params! Usage: php dump_users.php [filter value exact]\n";
	echo $err_msg;
	file_put_contents(ERROR_LOG_FILE, $err_msg, FILE_APPEND);
	die();	
}

?>
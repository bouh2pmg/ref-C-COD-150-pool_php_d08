<?php
include_once("db_pdo.php");


function my_password_change(PDO $bdd, $email, $new_password) {
	if(empty($new_password))
		throw new Exception;

	$password_hash = password_hash($new_password, PASSWORD_DEFAULT);
	if(password_verify($new_password, $password_hash))
	{
		$sql_query = "UPDATE users SET password=".$bdd->quote($password_hash)." WHERE email=".$bdd->quote($email)." LIMIT 1;";

		$result = $bdd->query($sql_query);
		if($result->rowCount() != 1)
			throw new Exception;
	}
}


$dbh = connect_db("localhost", "root", "root", "3306", "gecko");
if($dbh !== false)
{
	my_password_change($dbh, "hugo@hugo.com", "mouttom");
}
?>